//former name:on-lecture-下multer.ts
import express from 'express'
import dotenv from "dotenv";
dotenv.config();
import path from 'path'
import { Client } from "pg";
// import { readJsonConfigFile } from 'typescript'
// import jsonfile from 'jsonfile'
import multer from 'multer'

const tables = Object.freeze({
  reviews: "reviews",
  review_images: "review_images",
});

// type ReviewImage = {
//   id: number,
//   url: string,
//   review_id: number,
//   filename?: string | string[] | undefined; // or filename?:string
// }
type Review = {
  id: number;
  shop_id: number;
  user_id: number;
  review_text: string
  // ReviewImage: {}
}
// post-james':  no need (but why😖)
// const app = express()
// app.use(express.urlencoded({ extended: true }))
// app.use(express.json())
export const reviewRoutes = express.Router()
//----------------------------------------------------------------------------------

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve('./public/uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})
const upload = multer({ storage })

//----------------------------------------------------------------------------------
const client = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

client.connect();

//GET
reviewRoutes.get('/review/:shop_id', getReviews); // post-james, changed from app.get

async function getReviews(req: express.Request, res: express.Response) {
  // const shop_id = 
  // const user_id = ""
  const queryResult = await client.query(`SELECT * FROM ${tables.reviews} WHERE shop_id = $1 ORDER BY id DESC`, [req.params.shop_id]);
  // 🌟🌟alvin added: WHERE shop_id = $1 (before  ORDER BY id DESC`),[req.params.shop_id]);
  const reviews = queryResult.rows;
  res.json({ data: reviews });
}

reviewRoutes.post("/review", upload.array("uploads", 6), async function (req, res) {
  console.log(req.files, "<--req.files")
  console.log(req.body, "<--req.body")

  if (req.files) {
    for (let i = 0; i < req.files.length; i++) {

      let imagesPerReview = []
      let filename = req.files[i].filename
      imagesPerReview.push(filename)
    }
    const reviewText = req.body.reviewText; //ok
    const shop_id= req.body.shop_id
    
    console.log(reviewText, "<--reviewText");
    //const imagesPerReview = Array.from(req.files as { filename: string }[]).map(file => file.filename

    try {
      await client.query<Review>(/*sql*/ `INSERT INTO ${tables.reviews} (shop_id,user_id,review_text)VALUES($1,$2,$3)`, [shop_id, 1, reviewText])
      //await client.query(/*sql*/ `INSERT INTO ${tables.review_images} (url,review_id) VALUES($1,$2)`, [])
    } catch (err) {
      console.error(err);
    }
    res.json({ message: "success" });
  }
  else {
    res.json({ message: 'error in post route /review' });

  }
})

//----------------------------------------------------------------------------------
/*
app.post('/review', upload.array('profile', 6), async function (req, res) {
  console.log(req.files)// req.files is array of `photos` files
  console.log(req.body)// req.body will contain the text fields, if there were any
  const reviews: Review[] = await jsonfile.readFile(path.join(__dirname, 'datasets', 'reviews.json'))
  const reviewImage: ReviewImage = await jsonfile.readFile(path.join(__dirname, 'datasets', 'review-image.json'))
  if (req.files) {
    //req.files=array//req.files[0].filename-->.map
    // reviews.push({ id: reviews.length + 1, name: req.body.studentName, filename: Array.from(req.files as { filename: string }[]).map(file => file.filename) })
    reviews.push({
      id: reviews.length + 1, //review's id
      user_id: 1,
      shop_id: 1, //name: req.body.studentName,
      reviewImage: [
        {
          id: 1, //image's id
          url: "123",
          review_id: reviews.length + 1,
          filename: Array.from(req.files as { filename: string }[]).map(file => file.filename),
        }
      ]
    })
  } else {
    // reviews.push({ id: reviews.length + 1, name: req.body.studentName })
  }
  await jsonfile.writeFile(path.join(__dirname, 'datasets', 'reviews.json'), reviews)
  res.redirect('/home.html') //🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟
})
*/
//----------------------MUST NOT HAVE IN HERE NOW!!!!-------------------------------------
// app.use(express.static(path.join(__dirname, 'public')))

// const PORT = 8080
// app.listen(PORT, () => {
//   console.log(`listening to ${PORT}`)
// })






// app.post('/review', upload.single('profile'), async (req, res) => {
//   console.log(req.body) // form tag
//   console.log(req.file) // file info, not from req.body
//   const reviews: Review[] = await jsonfile.readFile(path.join(__dirname, 'datasets', 'reviews.json'))
//   reviews.push({ id: reviews.length + 1, name: req.body.studentName, filename: req.file?.filename })
//   await jsonfile.writeFile(path.join(__dirname, 'reviews.json'), reviews)
//   res.redirect('/home.html')
// })  // former route: students (fr wsp07)






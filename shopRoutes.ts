import express from 'express';
// import { client, io, shop, tables, upload} from './app';
import { client, tables, Shop } from './app';
export const shopRoutes = express.Router();
// focus one shop to create review
// shopRoutes.post('/:shop_id/review')

//GET
shopRoutes.get('/:id', getShop);
shopRoutes.get('/', getShops);

async function getShop(req: express.Request, res: express.Response) {
    console.log('getShop')

    // Get Shop Record by shop_id
    const shop_id = req.params.id

    try {

        const shop = (await client.query(/*SQL*/`SELECT * FROM shops WHERE id = $1;`, [shop_id])).rows[0];

        const shopImages = (await client.query(/*SQL*/`SELECT * FROM images WHERE shop_id = $1;`, [shop_id])).rows;

        const reviews = (await client.query(/*SQL*/`SELECT * FROM reviews JOIN users ON reviews.user_id = users.id WHERE reviews.shop_id = $1 ORDER BY reviews.id DESC`, [shop_id])).rows;

        console.log({ shop, shopImages, reviews });

        for (let index = 0; index < reviews.length; index++) {
            const review = reviews[index];
            const reviewImages = (await client.query(/*SQL*/`SELECT * FROM review_images WHERE review_id = $1 ORDER BY id DESC`, [review.id])).rows;
            reviews[index]['images'] = reviewImages
        }

        res.json({ shop, shopImages, reviews })
    } catch (err) {
        console.error(err)
        res.json({ result: false })
    }
}

async function getShops(req: express.Request, res: express.Response) {
    const queryResult = await client.query<Shop>(`SELECT * FROM ${tables.SHOP} ORDER BY id DESC`);
    const shops = queryResult.rows;
    res.json({ data: shops });
}

//POST
// shopRoutes.post('/index.html', upload.single("image"), postShops);

// export async function postShops(req: express.Request, res: express.Response) {
//     const content = req.body.content;
//     const filename = req.file?.filename;

//     console.log({content,filename})

//     await client.query(/*sql*/ `INSERT INTO ${tables.SHOP} (content, image) VALUES ($1, $2)`, [content, filename]);
//     io.emit("new-shop");
//     res.json({ message: "success" });
// }

//DELETE
// shopRoutes.delete('/shops/:mid', deleteShops);

// export async function deleteShops(req: express.Request, res: express.Response) {
//     const shopID = parseInt(req.params.mid);

//     console.log(shopID);

//     if (isNaN(shopID)) {
//       res.status(400).json({ message: "Invalid shop ID" });
//       return;
//     }
//     await client.query(/*sql*/ `DELETE FROM ${tables.SHOP} WHERE id = $1`, [shopID]);
//     res.json({ message: "success" });
// }

//UPDATE
// shopRoutes.put('/shops/:mid', updateShops);

// export async function updateShops(req: express.Request, res: express.Response) {
//     const id = parseInt(req.params.mid);


//     console.log(id);

//     if (isNaN(id)) {
//         res.status(400).json({ message: "id is not an integer" });
//         return;
//     }
//     await client.query(
//         `UPDATE shops set content = $1, updated_at = NOW() where id = $2`,
//         [req.body.content, id]
//     );
//     // io.to(`user-${req.session["user"].username}`).emit("shop-updated", {
//     //     message: "shop-updated!!",
//     // });
//     //res.json({ success: true });
//     res.json({ message: "success" });
// }
